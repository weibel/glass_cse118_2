package com.glassspeed;

/*
 * This Activity is used to detect the device the app is run on. Since there is no 
 *   set way to detect whether it is being run on Glass or a phone we have this screen
 *   which pops up a button and says to click it if you are on a phone. If you don't click
 *   it in 5 seconds a timer runs out and runs the Glass version of the app that displays 
 *   the info. If you hit the button then you go to PairActivity and get directions on 
 *   app usage. 
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;

public class DeviceSplitActivity extends Activity {

	private Button phoneButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_device_split);
		
		phoneButton = (Button) findViewById(R.id.PairButton);
		
		Global.onGlass = true;
		
		phoneButton.setOnClickListener(new View.OnClickListener(){
	  		public void onClick(View arg0) {
	  			
	  			Global.onGlass = false;
	  			
	  			Intent intent = new Intent(DeviceSplitActivity.this, PairActivity.class);
	  			startActivity(intent);
	  			
	  		}
		});
		
		CountDownTimer lTimer = new CountDownTimer(5000, 1000) {
	        public void onFinish() {
	           
	        	System.out.println("COUNTER JUST FINISHED");
	        	
	        	if(Global.onGlass){
	        		Intent intent = new Intent(DeviceSplitActivity.this, GlassSpeed.class);
	        		startActivity(intent);
	        		
	        		finish();
	        	}
	        		
	        }
			@Override
			public void onTick(long arg0) {
				// TODO Auto-generated method stub
			}
	    };
	    lTimer.start();
		
	}

}
