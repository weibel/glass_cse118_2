package com.glassspeed;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	//UI TextViews
	private TextView titleText;
	
	//UI Buttons
	private Button pairButton;
	private Button aboutButton;
	private Button exitButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.activity_main);
		
		titleText = (TextView) this.findViewById(R.id.TitleText);
		
		pairButton = (Button) this.findViewById(R.id.PairButton);
		aboutButton = (Button) this.findViewById(R.id.AboutButton);
		exitButton = (Button) this.findViewById(R.id.ExitButton);
		
		titleText.setText("Welcome to Glass Speed!");
		
		pairButton.setOnClickListener(new View.OnClickListener(){
	  		public void onClick(View arg0) {
	  			Intent intent = new Intent(MainActivity.this, PairActivity.class);
	  			startActivity(intent);
	  		}
		});
		
		aboutButton.setOnClickListener(new View.OnClickListener(){
	  		public void onClick(View arg0) {
	  			Intent intent = new Intent(MainActivity.this, AboutActivity.class);
	  			startActivity(intent);
	  		}
		});
		
		exitButton.setOnClickListener(new View.OnClickListener(){
	  		public void onClick(View arg0) {
	  			finish();
	  		}
		});
		
	}

}
