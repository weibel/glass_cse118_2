package com.glassspeed;

/*
 * This Activity just displays the directions for using the app. Actual directions are 
 *   specified in the layout files in the TextViews, here all that is being set
 *   are the actions of the run and cancel buttons. 
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class PairActivity extends Activity {

	private Button cancelButton;
	private Button runButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.activity_pair);
		
		runButton = (Button) this.findViewById(R.id.RunButton);
		cancelButton = (Button) this.findViewById(R.id.CancelButton);
		
		runButton.setOnClickListener(new View.OnClickListener(){
	  		public void onClick(View arg0) {
	  			
	  			Intent intent = new Intent(PairActivity.this, GlassSpeed.class);
	  			startActivity(intent);
	  			
	  		}
		});
		
		cancelButton.setOnClickListener(new View.OnClickListener(){
	  		public void onClick(View arg0) {
	  			finish();
	  		}
		});
		
	}
	
}
