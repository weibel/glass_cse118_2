/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.glassspeed;

import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.graphics.Matrix;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.Time;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


/**
 * This is the main Activity that displays the current chat session.
 */
public class BluetoothChat extends Activity implements LocationListener{
    // Debugging
    private static final String TAG = "BluetoothChat";
    private static final boolean D = true;

    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;

    // Layout Views
    private ListView mConversationView;
    private EditText mOutEditText;
    private Button mSendButton;

    // Name of the connected device
    private String mConnectedDeviceName = null;
    // String buffer for outgoing messages
    private StringBuffer mOutStringBuffer;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the chat services
    private BluetoothChatService mChatService = null;

    //Location variables
    private LocationManager locationManager;
	private String provider;
    
    //UI elements if on Google Glass
    private TextView speedText;
    private TextView distanceText;
    private TextView timeText;
    
    private TextView speedLargeText;
    
    // UI elements if on Andrid Phone
    private EditText editText;
    private Button button;
    
    private TextView distanceTextView;
    private TextView speedTextView;
    private TextView timeTextView;
   
    // Globals
	private Location prevLocation;
	private double distanceTraveled;
	private long prevTime;
	private long beginTime;
	private long totalTime;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        
        System.out.println("INSIDE BLUETOOTH CHAT ACTIVITY " + Global.onGlass);

        if(Global.onGlass){
        	
        	setContentView(R.layout.activity_display);

        	speedText = (TextView) this.findViewById(R.id.Speed);
        	timeText = (TextView) this.findViewById(R.id.Time);
        	distanceText = (TextView) this.findViewById(R.id.Distance);
        	
        	speedLargeText = (TextView) this.findViewById(R.id.SpeedLarge);
        	
        } else{
        	
        	setContentView(R.layout.activity_speed);
        	
        	locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    	    
    	    Criteria criteria = new Criteria();
    	    criteria.setPowerRequirement(Criteria.NO_REQUIREMENT);
    	    criteria.setAccuracy(Criteria.ACCURACY_FINE);
    	    provider = locationManager.getBestProvider(criteria, false);
    	    
    	    locationManager.requestLocationUpdates(provider, 500, 0, this);

    	    distanceTextView = (TextView) this.findViewById(R.id.DistanceText);
    	    speedTextView = (TextView) this.findViewById(R.id.SpeedText);
    	    timeTextView = (TextView) this.findViewById(R.id.TimeText);
    	    
        }
        
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if(D) Log.e(TAG, "++ ON START ++");

        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        // Otherwise, setup the chat session
        } else {
            if (mChatService == null) setupChat();
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        if(D) Log.e(TAG, "+ ON RESUME +");

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mChatService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mChatService.getState() == BluetoothChatService.STATE_NONE) {
              // Start the Bluetooth chat services
              mChatService.start();
            }
        }
    }

    private void setupChat() {
        Log.d(TAG, "setupChat()");

        /*
        // Initialize the array adapter for the conversation thread
        mConversationArrayAdapter = new ArrayAdapter<String>(this, R.layout.message);
        mConversationView = (ListView) findViewById(R.id.in);
        mConversationView.setAdapter(mConversationArrayAdapter);

        // Initialize the compose field with a listener for the return key
        mOutEditText = (EditText) findViewById(R.id.edit_text_out);
        mOutEditText.setOnEditorActionListener(mWriteListener);

        // Initialize the send button with a listener that for click events
        mSendButton = (Button) findViewById(R.id.button_send);
        mSendButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Send a message using content of the edit text widget
                TextView view = (TextView) findViewById(R.id.edit_text_out);
                String message = view.getText().toString();
                sendMessage(message);
            }
        });
		*/
        
        // Initialize the BluetoothChatService to perform bluetooth connections
        mChatService = new BluetoothChatService(this, mHandler);

        // Initialize the buffer for outgoing messages
        mOutStringBuffer = new StringBuffer("");
    }

    @Override
    public synchronized void onPause() {
        super.onPause();
        if(D) Log.e(TAG, "- ON PAUSE -");
    }

    @Override
    public void onStop() {
        super.onStop();
        if(D) Log.e(TAG, "-- ON STOP --");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth chat services
        if (mChatService != null) mChatService.stop();
        if(D) Log.e(TAG, "--- ON DESTROY ---");
    }

    private void ensureDiscoverable() {
        if(D) Log.d(TAG, "ensure discoverable");
        if (mBluetoothAdapter.getScanMode() !=
            BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }

    /**
     * Sends a message.
     * @param message  A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (mChatService.getState() != BluetoothChatService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            mChatService.write(send);

            // Reset out string buffer to zero and clear the edit text field
            mOutStringBuffer.setLength(0);
            //mOutEditText.setText(mOutStringBuffer);
        }
    }

    // The action listener for the EditText widget, to listen for the return key
    private TextView.OnEditorActionListener mWriteListener =
        new TextView.OnEditorActionListener() {
        public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
            // If the action is a key-up event on the return key, send the message
            if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_UP) {
                String message = view.getText().toString();
                sendMessage(message);
            }
            if(D) Log.i(TAG, "END onEditorAction");
            return true;
        }
    };

    private final void setStatus(int resId) {
        final ActionBar actionBar = getActionBar();
        actionBar.setSubtitle(resId);
    }

    private final void setStatus(CharSequence subTitle) {
        final ActionBar actionBar = getActionBar();
        actionBar.setSubtitle(subTitle);
    }

    // The Handler that gets information back from the BluetoothChatService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_STATE_CHANGE:
                if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                switch (msg.arg1) {
                case BluetoothChatService.STATE_CONNECTED:
                    setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));
                    
                    break;
                case BluetoothChatService.STATE_CONNECTING:
                    setStatus(R.string.title_connecting);
                    break;
                case BluetoothChatService.STATE_LISTEN:
                case BluetoothChatService.STATE_NONE:
                    setStatus(R.string.title_not_connected);
                    break;
                }
                break;
            case MESSAGE_WRITE:
                byte[] writeBuf = (byte[]) msg.obj;
                // construct a string from the buffer
                String writeMessage = new String(writeBuf);
                
                break;
            case MESSAGE_READ:
                byte[] readBuf = (byte[]) msg.obj;
                // construct a string from the valid bytes in the buffer
                String readMessage = new String(readBuf, 0, msg.arg1);
                
                String[] data = readMessage.split(";");
                
                /*
                caloriesText.setVisibility(View.GONE);
            	timeText.setVisibility(View.GONE);
            	text5.setVisibility(View.GONE);
            	distanceText.setVisibility(View.GONE);
            	
            	speedText.setVisibility(View.VISIBLE);
            	speedText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 120);
            	
            	speedText.setText(data[glassDisplay]);
                */
                
                if(Double.parseDouble(data[1]) >= 5.0){
                	
                	timeText.setVisibility(View.INVISIBLE);
                	distanceText.setVisibility(View.INVISIBLE);
                	speedText.setVisibility(View.INVISIBLE);
                	
                	speedLargeText.setVisibility(View.VISIBLE);
                	
                	speedText.setText(data[1] + " MPH");
                	speedLargeText.setText(data[1]);
                	
                } else{
                
                	timeText.setVisibility(View.VISIBLE);
                	distanceText.setVisibility(View.VISIBLE);
                	speedText.setVisibility(View.VISIBLE);
                	
                	speedLargeText.setVisibility(View.INVISIBLE);
                	
                	distanceText.setText(data[0]);
                    speedText.setText(data[1]);
                    speedLargeText.setText(data[1]);
                    timeText.setText(data[2]);
                	
                }
                
                //rotateNeedle(needle, 15.0, Double.parseDouble(data[1]));
                
                break;
            case MESSAGE_DEVICE_NAME:
                // save the connected device's name
                mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                Toast.makeText(getApplicationContext(), "Connected to "
                               + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                break;
            case MESSAGE_TOAST:
                Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                               Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(D) Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
        case REQUEST_CONNECT_DEVICE_SECURE:
            // When DeviceListActivity returns with a device to connect
            if (resultCode == Activity.RESULT_OK) {
                connectDevice(data, true);
            }
            break;
        case REQUEST_CONNECT_DEVICE_INSECURE:
            // When DeviceListActivity returns with a device to connect
            if (resultCode == Activity.RESULT_OK) {
                connectDevice(data, false);
            }
            break;
        case REQUEST_ENABLE_BT:
            // When the request to enable Bluetooth returns
            if (resultCode == Activity.RESULT_OK) {
                // Bluetooth is now enabled, so set up a chat session
                setupChat();
            } else {
                // User did not enable Bluetooth or an error occurred
                Log.d(TAG, "BT not enabled");
                Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        String address = data.getExtras()
            .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        mChatService.connect(device, secure);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent serverIntent = null;
        switch (item.getItemId()) {
        case R.id.secure_connect_scan:
            // Launch the DeviceListActivity to see devices and do scan
            serverIntent = new Intent(this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
            return true;
        case R.id.insecure_connect_scan:
            // Launch the DeviceListActivity to see devices and do scan
            serverIntent = new Intent(this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
            return true;
        case R.id.discoverable:
            // Ensure this device is discoverable by others
            ensureDiscoverable();
            return true;
        }
        return false;
    }

	@Override
	public void onLocationChanged(Location currLocation) { 
	   		
		if(prevLocation == null){
			prevLocation = locationManager.getLastKnownLocation(provider);
    	    prevTime = System.currentTimeMillis();
    	    beginTime = System.currentTimeMillis();
    	    distanceTraveled = 0.0;
    	    totalTime = 0;
    	    return;
		}
		
		double deltaDistance = (double) prevLocation.distanceTo(currLocation);
		distanceTraveled += deltaDistance;
		
		long currTime = System.currentTimeMillis();
		long deltaTime = currTime - prevTime;
		
		totalTime += deltaTime;
		
		float googleSpeed = currLocation.getSpeed();
		
		double speed = (double) googleSpeed * 2.23694;
		
		if(prevLocation.getLatitude() == currLocation.getLatitude() 
				&& prevLocation.getLongitude() == currLocation.getLongitude()){
			
			speed = 0.0;
			
		}
		
		prevLocation = currLocation;
		prevTime = currTime;
		
		DecimalFormat df = new DecimalFormat("#.##");
		
		double tempDistance = distanceTraveled * 0.000621371;
		
		double distanceRan = Double.valueOf(df.format(tempDistance));
		double speedRunning = Double.valueOf(df.format(speed));
		long timeRan = Long.valueOf(df.format(totalTime));
		
		Time time = new Time();
		time.set(totalTime);
		
		String distance = Double.toString(distanceRan);
		String speedString = Double.toString(speedRunning);
		//String totalTimeString = timeConversion(timeRan);
		String totalTimeString = timeConversion(timeRan);
		//String calories = calculateCalories(distanceRan, speedRunning, timeRan);
		
		distanceTextView.setText("Distance in miles = " + distance);
		timeTextView.setText("Speed in MPH = " + speedRunning);
		speedTextView.setText("timeRan = " + timeRan);
		
		sendMessage(distance + " Miles;" + speedString + ";" + totalTimeString + ";"); // + calories + " Calories");
		
	}
	
	private String calculateCalories(double distanceRan, double speedRunning, int timeRan){
		
		return "Calories";
		
	}
	
	private String distanceConversion(double distance){
		
		return Double.toString(distance * 0.000621371);
		
	}
	
	private String timeConversion(long seconds){
		
		String time = String.format("%d:%d", 
			    TimeUnit.MILLISECONDS.toMinutes(seconds),
			    TimeUnit.MILLISECONDS.toSeconds(seconds) - 
			    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(seconds))
			);
		
		String[] elements = time.split(":");
		
		String minutes = elements[0];
		String second = "";
		
		if(Integer.parseInt(elements[1]) < 10){
			
			 second = "0" + elements[1];
			
		}else{
			
			second = elements[1];
			
		}
		
		return minutes + ":" + second;
		
	}
	
	public void rotateNeedle(ImageView needle, double maxSpeed, double curSpeed) {

		double angle = 0.0;

		if (curSpeed <= maxSpeed) {

			angle = -90.0 + (curSpeed / maxSpeed) * 90.0;

		} else {

			angle = 0;

		}
		
		Matrix matrix=new Matrix();
		needle.setScaleType(ScaleType.MATRIX);   //required
		// matrix.postTranslate(-117, -117);
		matrix.postRotate((float) angle); // 117, 117 is center of needle
		// matrix.postTranslate(117, 117);
		needle.setImageMatrix(matrix);

	}
	
	/*
	private double calories(double mph, double weight){
		if(mph < 1.0) {
			return 0.0;
		} else {
			return ((3.5 + (mph * 5.36448)) * weight * 0.000035999);	
		}	
	}
	*/
	
	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

}
