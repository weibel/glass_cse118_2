package com.glassspeed;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SpeedActivity extends Activity implements LocationListener {
	
	private String milesPerHour;
	
	private TextView speed;
	private TextView location;
	
	private EditText editText;
	private Button button;
	
	private LocationManager locationManager;
	private String provider;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.activity_speed);
		
		speed = (TextView) this.findViewById(R.id.SpeedText);
		location = (TextView) this.findViewById(R.id.LocationText);
		
		speed.setText("SPEED");
		location.setText("LOCATION");
		
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	    
	    Criteria criteria = new Criteria();
	    criteria.setPowerRequirement(Criteria.NO_REQUIREMENT);
	    criteria.setAccuracy(Criteria.ACCURACY_FINE);
	    provider = locationManager.getBestProvider(criteria, false);
	    Location locationData = locationManager.getLastKnownLocation(provider);
		
	    locationManager.requestLocationUpdates(provider, 1000, 1, this);
	    
	    location.setText(locationData.toString());
	    
	    
		System.out.println("FINISHED onCreate()");
		
	}

	@Override
	public void onLocationChanged(Location newLocation) {
		
		System.out.println("In onLocationChanged()");
		
		milesPerHour = Integer.toString((int) (newLocation.getSpeed()*2.2369));
		
		location.setText(newLocation.toString());
		speed.setText(milesPerHour + " Miles / Hour");
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

}
